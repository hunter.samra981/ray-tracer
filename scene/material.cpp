#include "material.h"
#include "../ui/TraceUI.h"
#include "light.h"
#include "ray.h"
extern TraceUI *traceUI;

#include "../fileio/images.h"
#include <glm/gtx/io.hpp>
#include <iostream>

using namespace std;
extern bool debugMode;

Material::~Material() {}

// Apply the phong model to this point on the surface of the object, returning
// the color of that point.
glm::dvec3 Material::shade(Scene *scene, const ray &r, const isect &i) const {
  glm::dvec3 intensity = this->ke(i) + this->ka(i)*scene->ambient();
  for( Light* light : scene->getAllLights() ){
    // Light and shadow attentuation
    glm::dvec3 atten = light->distanceAttenuation(r.at(i)) * light->shadowAttenuation(r, r.at(i));

    ////////////////////////// Useful Variables //////////////////////////
    // Light Direction Vector (L for diffuse)
    glm::dvec3 L = light->getDirection(r.at(i));
    
    // Surface Normal Vector (N)
    glm::dvec3 N = i.getN();

    // Normalized Surface to Camera Vector (V)
    glm::dvec3 V = -r.getDirection();

    // Shininess Value (n)
    double n = this->shininess(i);

    // Normalized Reflection Vector
    glm::dvec3 R = (N * (2.0 * glm::dot(N,L))) - L;

    //////////////////////////////////////////////////////////////////////

    // diffuse term
    glm::dvec3 diffuse = this->kd(i) * std::max(0.0, glm::dot(L, N));

    // specular term
    glm::dvec3 specular = this->ks(i) * std::pow(std::max(0.0, glm::dot(R, V)), n);

    intensity = intensity + atten * (light->getColor() * (diffuse + specular));
  }
  return intensity;
}

TextureMap::TextureMap(string filename) {
  data = readImage(filename.c_str(), width, height);
  if (data.empty()) {
    width = 0;
    height = 0;
    string error("Unable to load texture map '");
    error.append(filename);
    error.append("'.");
    throw TextureMapException(error);
  }
}

glm::dvec3 TextureMap::getMappedValue(const glm::dvec2 &coord) const {
  int x = round(coord.x*width);
  int y = round(coord.y*height);
  return getPixelAt(x,y);
}

glm::dvec3 TextureMap::getPixelAt(int x, int y) const {
  int pixel_index = 3*(x + width * y);
  double r_value = data[pixel_index]/255.0;
  double g_value = data[pixel_index+1]/255.0;
  double b_value = data[pixel_index+2]/255.0;

  return glm::dvec3(r_value, g_value, b_value);
}

glm::dvec3 MaterialParameter::value(const isect &is) const {
  if (0 != _textureMap)
    return _textureMap->getMappedValue(is.getUVCoordinates());
  else
    return _value;
}

double MaterialParameter::intensityValue(const isect &is) const {
  if (0 != _textureMap) {
    glm::dvec3 value(_textureMap->getMappedValue(is.getUVCoordinates()));
    return (0.299 * value[0]) + (0.587 * value[1]) + (0.114 * value[2]);
  } else
    return (0.299 * _value[0]) + (0.587 * _value[1]) + (0.114 * _value[2]);
}
