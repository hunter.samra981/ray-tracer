#include <cmath>
#include <iostream>

#include "light.h"
#include <glm/glm.hpp>
#include <glm/gtx/io.hpp>

using namespace std;

double DirectionalLight::distanceAttenuation(const glm::dvec3 &) const {
  // distance to light is infinite, so f(di) goes to 0.  Return 1.
  return 1.0;
}

glm::dvec3 DirectionalLight::shadowAttenuation(const ray &r,
                                               const glm::dvec3 &p) const {
  glm::dvec3 d = -this->orientation;
  ray point_to_light = ray(p, d, glm::dvec3(1.0, 1.0, 1.0), ray::SHADOW);
  isect i;
  
  // There was an intersection going to the light source, shadow
  if (this->scene->intersect(point_to_light, i)) {
    return i.getMaterial().kt(i);
  }

  // No intersection, no shadow
  return glm::dvec3(1.0, 1.0, 1.0);
}

glm::dvec3 DirectionalLight::getColor() const { return color; }

glm::dvec3 DirectionalLight::getDirection(const glm::dvec3 &) const {
  return -orientation;
}

double PointLight::distanceAttenuation(const glm::dvec3 &P) const {
  double dist = glm::distance(this->position, P);
  return std::min(1.0, 1 / (this->constantTerm + (this->linearTerm * dist) + (this->quadraticTerm * std::pow(dist, 2))));
}

glm::dvec3 PointLight::getColor() const { return color; }

glm::dvec3 PointLight::getDirection(const glm::dvec3 &P) const {
  return glm::normalize(position - P);
}

glm::dvec3 PointLight::shadowAttenuation(const ray &r,
                                         const glm::dvec3 &p) const {
  glm::dvec3 d = getDirection(p);
  ray point_to_light = ray(p, d, glm::dvec3(1.0, 1.0, 1.0), ray::SHADOW);
  
  // Get the t value of the light along the path
  double light_t = glm::dot((this->position - p) / d, glm::dvec3(1.0/3.0, 1.0/3.0, 1.0/3.0));
  isect i;

  this->scene->intersect(point_to_light, i);

  // There was an intersection going to the light source, shadow
  if (i.getT() < light_t) {
    return i.getMaterial().kt(i);
  }

  // No intersection, no shadow
  return glm::dvec3(1.0, 1.0, 1.0);
}

#define VERBOSE 0

