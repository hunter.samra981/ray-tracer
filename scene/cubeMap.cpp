#include "cubeMap.h"
#include "../scene/material.h"
#include "../ui/TraceUI.h"
#include "ray.h"
#include <iostream>
extern TraceUI *traceUI;

using namespace std;

glm::dvec3 CubeMap::getColor(ray r) const {
  glm::dvec3 ray_dir = r.getDirection();
  double x_dir = ray_dir.x;
  double y_dir = ray_dir.y;
  double z_dir = ray_dir.z;
  
  // See what our primary direction is
  if (abs(x_dir) > abs(y_dir)) {
    if (abs(x_dir) > abs(z_dir)) {
      // X is primary, choose the right texture to map based on +, -
      if(x_dir > 0) {
        glm::dvec2 in_vec = glm::dvec2(z_dir, y_dir)/abs(x_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[0]->getMappedValue(in_vec);
      }
      else {
        glm::dvec2 in_vec = glm::dvec2(-z_dir, y_dir)/abs(x_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[1]->getMappedValue(in_vec);
      }
    }
    else {
      // Z is primary, choose the right texture to map based on +, -
      if(z_dir > 0) {
        glm::dvec2 in_vec = glm::dvec2(-x_dir, y_dir)/abs(z_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[5]->getMappedValue(in_vec);
      }
      else {
        glm::dvec2 in_vec = glm::dvec2(x_dir, y_dir)/abs(z_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[4]->getMappedValue(in_vec);
      }
    }
  }
  else {
    if (abs(y_dir) > abs(z_dir)) {
      // Y is primary, choose the right texture to map based on +, -
      if(y_dir > 0) {
        glm::dvec2 in_vec = glm::dvec2(z_dir, x_dir)/abs(y_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[2]->getMappedValue(in_vec);
      }
      else {
        glm::dvec2 in_vec = glm::dvec2(-z_dir, x_dir)/abs(y_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[3]->getMappedValue(in_vec);
      }
    }
    else {
      // Z is primary, choose the right texture to map based on +, -
      if(z_dir > 0) {
        glm::dvec2 in_vec = glm::dvec2(-x_dir, y_dir)/abs(z_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[5]->getMappedValue(in_vec);
      }
      else {
        glm::dvec2 in_vec = glm::dvec2(x_dir, y_dir)/abs(z_dir);
        in_vec += 1;
        in_vec /= 2;
        return tMap[4]->getMappedValue(in_vec);
      }
    }
  }
  return glm::dvec3();
}

CubeMap::CubeMap() {}

CubeMap::~CubeMap() {}

void CubeMap::setNthMap(int n, TextureMap *m) {
  if (m != tMap[n].get())
    tMap[n].reset(m);
}
